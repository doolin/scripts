

# Some learning resources:
# http://en.wikipedia.org/wiki/Practice_(learning_method)
# http://en.wikipedia.org/wiki/Dreyfus_model_of_skill_acquisition
# http://en.wikipedia.org/wiki/Chris_Argyris
# http://en.wikipedia.org/wiki/Four_stages_of_competence
# http://en.wikipedia.org/wiki/Reflective_practice



# From Pickaxe 15.6, pp. 224-227

def delete(pattern)
  files = Dir[pattern]
  rm(files, :verbose => true) unless files.empty?
end

desc "Remove files ending with tilde~"
task :delete_unix_backups do
  delete '*~'
end

desc "Remove files with a .bak extension"
task :delete_windows_backups do
  delete '*.bak'
end

desc "Remove unix and Windows backup files"
task :delete_backups => [:delete_unix_backups, :delete_windows_backups] do
  puts "All backups deleted."
end
