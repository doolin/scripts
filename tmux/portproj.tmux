#!/bin/sh

tmux has-session -t portproj 2> /dev/null

if [ $? != 0 ]
then

  tmux new-session -s portproj -n editing -d

  tmux send-keys -t portproj 'cd ~/src/portfolio-project' C-m
  # tmux split-window -v -p 35 -t portproj
  tmux split-window -h -p 50 -t portproj

  tmux send-keys -t portproj:0.1 "cd ~/src/portfolio-project" C-m
  tmux send-keys -t portproj:0.2 "cd ~/src/portfolio-project" C-m

  # tmux send-keys -t portproj:0.1
  tmux select-pane -t portproj:0.0

  tmux new-window -t portproj -n server
  tmux send-keys -t portproj:server 'cd ~/src/portfolio-project' C-m
  tmux send-keys -t portproj:server 'rails s' C-m

  tmux new-window -t portproj -n spec
  tmux send-keys -t portproj:spec 'cd ~/src/portfolio-project' C-m

  tmux new-window -t portproj -n postgres
  tmux send-keys -t portproj:postgres 'cd ~/src/portfolio-project' C-m

  tmux select-window -t portproj:editing
  tmux select-pane -t portproj:editing.0
fi
tmux attach-session -t portproj
