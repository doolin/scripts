#!/bin/sh

tmux has-session -t tasklets 2> /dev/null

if [ $? != 0 ]
then

  export dir='~/src/tasklets'

  tmux new-session -s tasklets -n editing -d
  tmux send-keys -t tasklets "cd $dir" C-m
  tmux split-window -v -p 35 -t tasklets
  tmux split-window -v -p 50 -t tasklets
  tmux send-keys -t tasklets:0.1 "cd $dir" C-m
  tmux send-keys -t tasklets:0.2 "cd $dir" C-m
  tmux send-keys -t tasklets:0.2 'spork' C-m
  #tmux send-keys -t tasklets:0.1 # change to sass 'middleman' C-m
  tmux select-pane -t tasklets:0.0
#  tmux send-keys -t tasklets:0.0 'vi .' C-m

  tmux new-window -t tasklets -n server
  tmux send-keys -t tasklets:server "cd $dir" C-m
  tmux send-keys -t tasklets:server 'rails s' C-m

  tmux new-window -t tasklets -n spec
  tmux send-keys -t tasklets:spec "cd $dir" C-m

  #tmux new-window -t tasklets -n postgres
  #tmux send-keys -t tasklets:postgres "cd $dir" C-m
  #tmux send-keys -t tasklets:server 'psql tasklets_development' C-m

  tmux new-window -t tasklets -n guard-jasmine
  tmux send-keys -t tasklets:guard-jasmine "cd $dir" C-m
  tmux send-keys -t tasklets:server 'guard-jasmine' C-m

  tmux new-window -t tasklets -n environment
  tmux send-keys -t tasklets:environment "cd ~" C-m

  tmux select-window -t tasklets:editing
  tmux select-pane -t tasklets:editing.0
fi
tmux attach-session -t tasklets
