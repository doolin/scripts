#!/bin/sh

tmux has-session -t rubymacros 2> /dev/null

if [ $? != 0 ]
then
  tmux new-session -s rubymacros -n editing -d
  tmux send-keys -t rubymacros 'cd ~/src/rubymacros.com' C-m
  tmux split-window -v -p 25 -t rubymacros
  tmux send-keys -t rubymacros:0.1 'cd ~/src/rubymacros.com' C-m
  #tmux send-keys -t rubymacros:0.0 'vi .' C-m

  tmux new-window -t rubymacros -n server
  tmux send-keys -t rubymacros:server 'cd ~/src/rubymacros.com' C-m
  tmux send-keys -t rubymacros:server 'middleman --port 4568' C-m

  tmux new-window -t rubymacros -n sass
  tmux send-keys -t rubymacros:sass.0 'cd ~/src/rubymacros.com' C-m
  tmux send-keys -t rubymacros:sass.0 'sass --watch source/stylesheets --style expanded' C-m

  tmux new-window -t rubymacros -n environment
  tmux send-keys -t rubymacros:environment.0 'cd ~' C-m
  tmux select-window -t rubymacros:editing

  tmux select-pane -t rubymacros:editing.0
fi
tmux attach-session -t rubymacros
