#!/bin/sh

tmux has-session -t linebacker 2> /dev/null

if [ $? != 0 ]
then
  tmux new-session -s linebacker -n editing -d
  tmux send-keys -t linebacker 'cd ~/src/linebacker' C-m
  tmux split-window -v -p 25 -t linebacker
  tmux send-keys -t linebacker:0.1 'cd ~/src/linebacker' C-m
  #tmux send-keys -t linebacker:0.0 'vi .' C-m

  tmux new-window -t linebacker -n server
  tmux send-keys -t linebacker:server 'cd ~/src/linebacker' C-m
  # change to sinatra server
  # tmux send-keys -t linebacker:server 'ruby app.rb' C-m

  tmux new-window -t linebacker -n sass
  tmux send-keys -t linebacker:sass.0 'cd ~/src/linebacker' C-m
  tmux send-keys -t linebacker:sass.0 'sass --watch source/stylesheets --style expanded' C-m

  tmux new-window -t linebacker -n environment
  tmux send-keys -t linebacker:environment.0 'cd ~' C-m
  tmux select-window -t linebacker:editing

  tmux select-pane -t linebacker:editing.0
fi
tmux attach-session -t linebacker
