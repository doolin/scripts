#!/bin/sh

tmux has-session -t statustar 2> /dev/null

if [ $? != 0 ]
then

  export dir='~/src/statustar'

  tmux new-session -s statustar -n editing -d
  tmux send-keys -t statustar "cd $dir" C-m
  tmux split-window -v -p 35 -t statustar
  tmux split-window -v -p 50 -t statustar
  tmux send-keys -t statustar:0.1 "cd $dir" C-m
  tmux send-keys -t statustar:0.2 "cd $dir" C-m
  tmux send-keys -t statustar:0.2 'spork' C-m
  #tmux send-keys -t statustar:0.1 # change to sass 'middleman' C-m
  tmux select-pane -t statustar:0.0
#  tmux send-keys -t statustar:0.0 'vi .' C-m

  tmux new-window -t statustar -n server
  tmux send-keys -t statustar:server "cd $dir" C-m
  tmux send-keys -t statustar:server 'rails s' C-m

  tmux new-window -t statustar -n spec
  tmux send-keys -t statustar:spec "cd $dir" C-m

  tmux new-window -t statustar -n postgres
  tmux send-keys -t statustar:postgres "cd $dir" C-m
  tmux send-keys -t statustar:server 'psql statustar_development' C-m

  tmux new-window -t statustar -n guard-jasmine
  tmux send-keys -t statustar:guard-jasmine "cd $dir" C-m
  tmux send-keys -t statustar:server 'guard-jasmine' C-m

  tmux new-window -t statustar -n environment
  tmux send-keys -t statustar:environment "cd ~" C-m

  tmux select-window -t statustar:editing
  tmux select-pane -t statustar:editing.0
fi
tmux attach-session -t statustar
