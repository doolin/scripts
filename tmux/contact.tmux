#!/bin/sh

tmux has-session -t contact 2> /dev/null

if [ $? != 0 ]
then

  #export gemset='ruby-2.0.0-p195@contact'
  export gemset='ruby-1.9.3-p385@contact'
  export dir='~/src/contact'

  tmux new-session -s contact -n editing -d
  tmux send-keys -t contact "cd $dir" C-m

  # tmux send-keys -t contact "rvm use $gemset" C-m

  # tmux split-window -v -p 35 -t contact
  # tmux split-window -v -p 50 -t contact
  tmux split-window -h -p 50 -t contact

  tmux send-keys -t contact:0.1 "cd $dir" C-m
  # tmux send-keys -t contact:0.2 "cd $dir" C-m
  #tmux send-keys -t contact:0.1 # change to sass 'middleman' C-m
  tmux select-pane -t contact:0.0
#  tmux send-keys -t contact:0.0 'vi .' C-m

  tmux new-window -t contact -n server
  tmux send-keys -t contact:server "cd $dir" C-m
  tmux send-keys -t contact:server 'rails s' C-m

  # tmux new-window -t contact -n spec
  # tmux send-keys -t contact:spec "cd $dir" C-m

  # tmux new-window -t contact -n postgres
  # tmux send-keys -t contact:postgres "cd $dir" C-m
  # tmux send-keys -t contact:server 'psql contact_development' C-m

  # tmux new-window -t contact -n environment
  # tmux send-keys -t contact:environment "cd ~" C-m

  # tmux select-window -t contact:editing
  # tmux select-pane -t contact:editing.0
fi
tmux attach-session -t contact
