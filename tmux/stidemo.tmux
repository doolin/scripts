#!/bin/sh

tmux has-session -t stidemo 2> /dev/null

if [ $? != 0 ]
then
  tmux new-session -s stidemo -n editing -d

  tmux send-keys -t stidemo 'cd ~/src/stidemo' C-m
  tmux send-keys -t stidemo 'rvm use ruby-2.0.0-p0@stidemo' C-m
  tmux split-window -v -p 25 -t stidemo:editing
  tmux send-keys -t stidemo:editing.1 'cd ~/src/stidemo' C-m
  tmux send-keys -t stidemo:editing.1 'rvm use ruby-2.0.0-p0@stidemo' C-m
  #tmux send-keys -t stidemo:0.1 'middleman' C-m
  #tmux send-keys -t stidemo:0.0 'vi .' C-m

  tmux new-window -t stidemo -n server
  tmux send-keys -t stidemo 'cd ~/src/stidemo' C-m
  tmux send-keys -t stidemo:server.0 'rvm use ruby-2.0.0-p0@stidemo' C-m
  tmux send-keys -t stidemo:server.0 'rails server' C-m

  tmux new-window -t stidemo -n sass
  tmux send-keys -t stidemo 'cd ~/src/stidemo' C-m
  tmux send-keys -t stidemo:sass.0 'rvm use ruby-2.0.0-p0@stidemo' C-m
  tmux send-keys -t stidemo:sass.0 'sass --watch app/assets/stylesheets --style expanded' C-m

  tmux select-pane -t stidemo:editing.top
  tmux select-window -t stidemo:editing
fi
tmux attach-session -t stidemo
