#!/bin/sh

tmux has-session -t cabochons 2> /dev/null

if [ $? != 0 ]
then
  tmux new-session -s cabochons -n editing -d
  tmux send-keys -t cabochons 'cd ~/src/cabochons' C-m
  tmux split-window -v -p 25 -t cabochons
  tmux send-keys -t cabochons:0.1 'cd ~/src/cabochons' C-m
  #tmux send-keys -t cabochons:0.0 'vi .' C-m


  tmux new-window -t cabochons -n server
  tmux send-keys -t cabochons:server 'cd ~/src/cabochons' C-m
  tmux send-keys -t cabochons:server 'yard server' C-m


  tmux new-window -t cabochons -n console
  tmux send-keys -t cabochons:console.0 'cd ~/src/cabochons' C-m
  # Consider changing to pry
  tmux send-keys -t cabochons:console.0 'irb' C-m

#  tmux new-window -t cabochons -n mastery
#  tmux send-keys -t cabochons:mastery.0 'cd ~/src/cabochons/source/mastery' C-m

  tmux new-window -t cabochons -n environment
  tmux send-keys -t cabochons:environment.0 'cd ~' C-m

  tmux select-window -t cabochons:editing
  tmux select-pane -t cabochons:editing.0
fi
tmux attach-session -t cabochons
