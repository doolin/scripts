#!/bin/sh

export dir='~/src/fijournal'

tmux has-session -t fijournal 2> /dev/null


if [ $? != 0 ]
then
  tmux new-session -s fijournal -n daily -d
  tmux send-keys -t fijournal 'cd ~/src/fijournal' C-m
  tmux split-window -v -p 25 -t fijournal
  tmux send-keys -t fijournal:daily.1 'cd ~/src/fijournal' C-m

  tmux new-window -t fijournal -n server
  tmux send-keys -t fijournal:server 'cd ~/src/fijournal' C-m
  tmux send-keys -t fijournal:server 'middleman' C-m


  tmux new-window -t fijournal -n sass
  tmux send-keys -t fijournal:sass.0 'cd ~/src/fijournal' C-m
  tmux send-keys -t fijournal:sass.0 'sass --watch source/stylesheets --style expanded' C-m

  tmux new-window -t fijournal -n security
  tmux send-keys -t fijournal:security 'cd ~/src/fijournal/source/security' C-m

  tmux new-window -t fijournal -n mastery
  tmux send-keys -t fijournal:mastery.0 'cd ~/src/fijournal/source/mastery' C-m

  tmux new-window -t fijournal -n career
  tmux send-keys -t fijournal:career.0 'cd ~/src/fijournal/source/career' C-m

  tmux new-window -t fijournal -n data
  tmux send-keys -t fijournal:data 'cd ~/src/fijournal/data' C-m

  tmux new-window -t fijournal -n katas
  tmux send-keys -t fijournal:katas.0 'cd ~/src/fijournal/source/katas' C-m

  tmux new-window -t fijournal -n orgmode
  tmux send-keys -t fijournal:orgmode.0 'cd ~/src/fijournal/source/orgmode' C-m

  tmux new-window -t fijournal -n operations
  tmux send-keys -t fijournal:operations.0 'cd ~/src/fijournal/source/operations' C-m

  tmux new-window -t fijournal -n lib
  tmux send-keys -t fijournal:lib.0 'cd ~/src/fijournal/lib' C-m

  tmux new-window -t fijournal -n site
  tmux send-keys -t fijournal:site.0 'cd ~/src/fijournal/source' C-m

  tmux new-window -t fijournal -n console
  tmux send-keys -t fijournal:console.0 'cd ~/src/fijournal' C-m
  # tmux send-keys -t fijournal:console.0 'irb' C-m

  tmux new-window -t fijournal -n vimhelp
  tmux send-keys -t fijournal:vimhelp.0 'cd ~' C-m

  tmux new-window -t fijournal -n environment
  tmux send-keys -t fijournal:environment.0 'cd ~' C-m

  tmux select-window -t fijournal:daily
  tmux select-pane -t fijournal:daily.0
fi

tmux attach-session -t fijournal
