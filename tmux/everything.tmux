#!/bin/sh

tmux has-session -t everything 2> /dev/null

if [ $? != 0 ]
then

  tmux new-session -s everything -n statustar -d
  tmux split-window -h -p 50 -t statustar
  tmux send-keys -t everything:statustar.0 "cd ~/src/statustar" C-m
  tmux send-keys -t everything:statustar.1 "cd ~/src/statustar" C-m

  tmux new-window -t everything -n tasklets -d
  tmux split-window -h -p 50 -t tasklets
  tmux send-keys -t everything:tasklets.0 "cd ~/src/tasklets" C-m
  tmux send-keys -t everything:tasklets.1 "cd ~/src/tasklets" C-m

  tmux new-window -t everything -n contact -d
  tmux split-window -h -p 50 -t contact
  tmux send-keys -t everything:contact.0 "cd ~/src/contact" C-m
  tmux send-keys -t everything:contact.1 "cd ~/src/contact" C-m

  tmux new-window -t everything -n stormsavvy -d
  tmux send-keys -t everything:stormsavvy "cd ~/src/stormsavvy" C-m

  tmux new-window -t everything -n portproj -d
  tmux split-window -h -p 50 -t portproj
  tmux send-keys -t everything:portproj.0 "cd ~/src/portfolio-project" C-m
  tmux send-keys -t everything:portproj.1 "cd ~/src/portfolio-project" C-m

  tmux new-window -t everything -n arw -d
  tmux split-window -h -p 50 -t arw
  tmux send-keys -t everything:arw.0 "cd ~/src/arw" C-m
  tmux send-keys -t everything:arw.1 "cd ~/src/arw" C-m

  tmux new-window -t everything -n dotfiles -d
  tmux send-keys -t everything:dotfiles "cd ~/src/dotfiles" C-m

  tmux new-window -t everything -n linebacker -d
  tmux send-keys -t everything:linebacker "cd ~/src/linebacker" C-m

  tmux new-window -t everything -n clickable-laos -d
  tmux send-keys -t everything:clickable-laos "cd ~/src/clickable-laos" C-m

  tmux new-window -t everything -n data-science-from-scratch -d
  tmux split-window -h -p 50 -t data-science-from-scratch
  tmux send-keys -t everything:data-science-from-scratch.0 "cd ~/src/data-science-from-scratch" C-m
  tmux send-keys -t everything:data-science-from-scratch.1 "cd ~/src/data-science-from-scratch" C-m

  tmux new-window -t everything -n rebuilding-rails -d
  tmux split-window -h -p 50 -t rebuilding-rails
  tmux send-keys -t everything:rebuilding-rails.0 "cd ~/src/rebuilding-rails" C-m
  tmux send-keys -t everything:rebuilding-rails.1 "cd ~/src/rebuilding-rails" C-m

  tmux new-window -t everything -n rails-wizards -d
  tmux split-window -h -p 50 -t rails-wizards
  tmux send-keys -t everything:rails-wizards.0 "cd ~/src/rails-wizards" C-m
  tmux send-keys -t everything:rails-wizards.1 "cd ~/src/rails-wizards" C-m

  tmux new-window -t everything -n haskell -d
  tmux send-keys -t everything:haskell "cd ~/src/haskell" C-m

  tmux new-window -t everything -n erlang -d
  tmux send-keys -t everything:erlang "cd ~/src/erlang" C-m

  tmux new-window -t everything -n elixir -d
  tmux send-keys -t everything:elixir "cd ~/src/elixir" C-m

  tmux new-window -t everything -n programming-elixir -d
  tmux send-keys -t everything:programming-elixir "cd ~/src/programming-elixir" C-m

  tmux new-window -t everything -n metaprogramming-elixir -d
  tmux send-keys -t everything:metaprogramming-elixir "cd ~/src/metaprogramming-elixir" C-m

  tmux new-window -t everything -n programming-phoenix -d
  tmux send-keys -t everything:programming-phoenix "cd ~/src/programming-phoenix" C-m

  tmux new-window -t everything -n mastering-postgres -d
  tmux send-keys -t everything:mastering-postgres "cd ~/src/mastering-postgres" C-m

  # These are finished
  # tmux new-window -t everything -n elixir-koans -d
  # tmux send-keys -t everything:elixir-koans "cd ~/src/elixir-koans" C-m

  # tmux new-window -t everything -n taking-off -d
  # tmux send-keys -t everything:taking-off "cd ~/src/taking-off-with-elixir" C-m

  tmux new-window -t everything -n crystal -d
  tmux send-keys -t everything:crystal "cd ~/src/crystal" C-m

  tmux new-window -t everything -n doolin -d
  tmux send-keys -t everything:doolin "cd ~/src/dool.in" C-m

  tmux new-window -t everything -n testbed -d
  tmux send-keys -t everything:testbed "cd ~/src/testbed" C-m

  tmux new-window -t everything -n tinobox -d
  tmux send-keys -t everything:tinobox "cd ~/src/tinobox/src" C-m

  tmux new-window -t everything -n fitl -d
  tmux send-keys -t everything:fitl "cd ~/src/fitl.rb" C-m

  tmux new-window -t everything -n ntdda -d
  tmux send-keys -t everything:ntdda "cd ~/src/ntdda" C-m

  tmux new-window -t everything -n arscope -d
  tmux send-keys -t everything:arscope "cd ~/src/arscope" C-m

  tmux new-window -t everything -n confsched -d
  tmux send-keys -t everything:confsched "cd ~/src/confsched" C-m

  tmux new-window -t everything -n potd -d
  tmux send-keys -t everything:potd "cd ~/src/potd" C-m

  tmux new-window -t everything -n rosen -d
  tmux send-keys -t everything:rosen "cd ~/src/rosen" C-m

  tmux new-window -t everything -n kestrels -d
  tmux send-keys -t everything:kestrels "cd ~/src/kestrels-and-quirky-birds" C-m

  tmux new-window -t everything -n cabochons -d
  tmux send-keys -t everything:cabochons "cd ~/src/cabochons" C-m

  tmux new-window -t everything -n exercism -d
  tmux send-keys -t everything:exercism "cd ~/exercism" C-m

  tmux new-window -t everything -n ctpi -d
  tmux send-keys -t everything:ctpi "cd ~/src/ctpi" C-m

  tmux new-window -t everything -n autotoolerpp -d
  tmux send-keys -t everything:autotoolerpp "cd ~/src/autotoolerpp" C-m

  tmux new-window -t everything -n projecteuler -d
  tmux send-keys -t everything:projecteuler "cd ~/src/testbed/projecteuler" C-m

  tmux new-window -t everything -n sicp -d
  tmux send-keys -t everything:sicp "cd ~/src/sicp" C-m

  tmux new-window -t everything -n sorting -d
  tmux send-keys -t everything:sorting "cd ~/src/sorting" C-m

  tmux new-window -t everything -n bstree -d
  tmux split-window -h -p 50 -t bstree
  tmux send-keys -t everything:bstree.0 "cd ~/src/bstree" C-m
  tmux send-keys -t everything:bstree.1 "cd ~/src/bstree" C-m

  tmux new-window -t everything -n rubymacros -d
  tmux split-window -h -p 50 -t rubymacros
  tmux send-keys -t everything:rubymacros.0 "cd ~/src/rubymacros.com" C-m
  tmux send-keys -t everything:rubymacros.1 "cd ~/src/rubymacros.com" C-m

  tmux new-window -t everything -n clrs -d
  tmux send-keys -t everything:clrs "cd ~/src/clrs" C-m

  tmux new-window -t everything -n resume -d
  tmux send-keys -t everything:resume "cd ~/src/tinobox/administrivia/resume" C-m

  # Finished with this book for now, may go back into it in the
  # future so having this available would be handy.
  # tmux new-window -t everything -n design-patterns-ruby -d
  # tmux split-window -h -p 50 -t design-patterns-ruby
  # tmux send-keys -t everything:design-patterns-ruby.0 "cd ~/src/design-patterns-in-ruby" C-m
  # tmux send-keys -t everything:design-patterns-ruby.1 "cd ~/src/design-patterns-in-ruby" C-m

  tmux new-window -t everything -n mastering-ruby-strings -d
  tmux split-window -h -p 50 -t mastering-ruby-strings
  tmux send-keys -t everything:mastering-ruby-strings.0 "cd ~/src/mastering-ruby-strings" C-m
  tmux send-keys -t everything:mastering-ruby-strings.1 "cd ~/src/mastering-ruby-strings" C-m

  tmux new-window -t everything -n heapsort -d
  tmux send-keys -t everything:heapsort "cd ~/src/heapsort" C-m

  tmux new-window -t everything -n asdftextgenerator -d
  tmux send-keys -t everything:asdftextgenerator "cd ~/src/asdftextgenerator" C-m

  tmux new-window -t everything -n fxtbook -d
  tmux send-keys -t everything:fxtbook "cd ~/src/fxtbook" C-m

  tmux new-window -t everything -n agile51 -d
  tmux send-keys -t everything:agile51 "cd ~/src/agile51" C-m

  tmux new-window -t everything -n latexcolor -d
  tmux send-keys -t everything:latexcolor "cd ~/src/latexcolor.com" C-m

  tmux new-window -t everything -n tmux_scripts -d
  tmux send-keys -t everything:tmux_scripts "cd ~/scripts/tmux" C-m

fi
tmux attach-session -t everything
