#!/bin/sh

tmux has-session -t cacklist 2> /dev/null

if [ $? != 0 ]
then

  #export gemset='ruby-2.0.0-p195@cacklist'
  export gemset='ruby-1.9.3-p385@cacklist'
  export dir='~/src/cacklist'

  tmux new-session -s cacklist -n editing -d
  tmux send-keys -t cacklist "cd $dir" C-m
  tmux send-keys -t cacklist "rvm use $gemset" C-m
  tmux split-window -v -p 35 -t cacklist
  tmux split-window -v -p 50 -t cacklist
  tmux send-keys -t cacklist:0.1 "cd $dir" C-m
  tmux send-keys -t cacklist:0.2 "cd $dir" C-m
  tmux send-keys -t cacklist:0.1 "rvm use $gemset" C-m
  tmux send-keys -t cacklist:0.2 "rvm use $gemset" C-m
  tmux send-keys -t cacklist:0.2 'spork' C-m
  #tmux send-keys -t cacklist:0.1 # change to sass 'middleman' C-m
  tmux select-pane -t cacklist:0.0
#  tmux send-keys -t cacklist:0.0 'vi .' C-m

  tmux new-window -t cacklist -n server
  tmux send-keys -t cacklist:server "cd $dir" C-m
  tmux send-keys -t cacklist:server "rvm use $gemset" C-m
  tmux send-keys -t cacklist:server 'rails s' C-m

  tmux new-window -t cacklist -n spec
  tmux send-keys -t cacklist:spec "cd $dir" C-m
  tmux send-keys -t cacklist:spec "rvm use $gemset" C-m

  tmux new-window -t cacklist -n postgres
  tmux send-keys -t cacklist:postgres "cd $dir" C-m
  tmux send-keys -t cacklist:postgres "rvm use $gemset" C-m
  tmux send-keys -t cacklist:server 'psql cacklist_development' C-m

  tmux new-window -t cacklist -n guard-jasmine
  tmux send-keys -t cacklist:guard-jasmine "cd $dir" C-m
  tmux send-keys -t cacklist:guard-jasmine "rvm use $gemset" C-m
  tmux send-keys -t cacklist:server 'guard-jasmine' C-m

  tmux new-window -t cacklist -n environment
  tmux send-keys -t cacklist:environment "cd ~" C-m
  tmux send-keys -t cacklist:environment "rvm use $gemset" C-m

  tmux select-window -t cacklist:editing
  tmux select-pane -t cacklist:editing.0
fi
tmux attach-session -t cacklist
