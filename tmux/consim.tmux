#!/bin/sh

export dir='~/src/consim'

tmux has-session -t consim 2> /dev/null


if [ $? != 0 ]
then
  tmux new-session -s consim -n daily -d
  tmux send-keys -t consim "cd $dir" C-m
  tmux split-window -v -p 25 -t consim
  tmux send-keys -t consim:daily.1  "echo http://localhost:4568/"
  tmux send-keys -t consim:daily.1 "cd $dir" C-m

  tmux new-window -t consim -n server
  tmux send-keys -t consim:server "cd $dir" C-m
  tmux send-keys -t consim:server "middleman -p4568" C-m

  tmux new-window -t consim -n sass
  tmux send-keys -t consim:sass.0 "cd $dir" C-m
  tmux send-keys -t consim:sass.0 "sass --watch source/stylesheets --style expanded" C-m

  tmux new-window -t consim -n ode
  tmux send-keys -t consim:ode "cd $dir/source/ode" C-m

  tmux new-window -t consim -n pb
  tmux send-keys -t consim:pb "cd $dir/source/pb" C-m

  tmux new-window -t consim -n data
  tmux send-keys -t consim:data "cd $dir/data" C-m

  tmux new-window -t consim -n lib
  tmux send-keys -t consim:lib.0 "cd $dir/lib" C-m

  tmux new-window -t consim -n site
  tmux send-keys -t consim:site.0 "cd $dir/source" C-m

  tmux select-window -t consim:daily
  tmux select-pane -t consim:daily.0
fi
tmux attach-session -t consim
