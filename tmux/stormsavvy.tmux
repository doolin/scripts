#!/bin/sh

tmux has-session -t stormsavvy 2> /dev/null

if [ $? != 0 ]
then
  tmux new-session -s stormsavvy -n editing -d
  tmux send-keys -t stormsavvy 'cd ~/src/stormsavvy' C-m
  tmux send-keys -t stormsavvy 'rvm use 2.0.0-p353@stormsavvy' C-m
  tmux split-window -v -p 35 -t stormsavvy
  tmux split-window -v -p 50 -t stormsavvy
  tmux send-keys -t stormsavvy:0.1 'cd ~/src/stormsavvy' C-m
  tmux send-keys -t stormsavvy:0.2 'cd ~/src/stormsavvy' C-m
  tmux send-keys -t stormsavvy:0.1 'rvm use 2.0.0-p353@stormsavvy' C-m
  tmux send-keys -t stormsavvy:0.2 'rvm use 2.0.0-p353@stormsavvy' C-m
  tmux send-keys -t stormsavvy:0.2 'spork' C-m
  #tmux send-keys -t stormsavvy:0.1 # change to sass 'middleman' C-m
  tmux select-pane -t stormsavvy:0.0
#  tmux send-keys -t stormsavvy:0.0 'vi .' C-m

  tmux new-window -t stormsavvy -n server
  tmux send-keys -t stormsavvy:server 'cd ~/src/stormsavvy' C-m
  tmux send-keys -t stormsavvy:server 'rvm use ruby-2.0.0-p353@stormsavvy' C-m
  tmux send-keys -t stormsavvy:server 'rails s' C-m

  tmux new-window -t stormsavvy -n spec
  tmux send-keys -t stormsavvy:spec 'cd ~/src/stormsavvy' C-m
  tmux send-keys -t stormsavvy:spec 'rvm use ruby-2.0.0-p353@stormsavvy' C-m

  tmux select-window -t stormsavvy:editing
  tmux select-pane -t stormsavvy:editing.0
fi
tmux attach-session -t stormsavvy
