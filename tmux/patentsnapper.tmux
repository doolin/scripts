#!/bin/sh

tmux has-session -t patentserver 2> /dev/null

if [ $? != 0 ]
then
  tmux new-session -s patentserver -n editing -d
  tmux send-keys -t patentserver 'cd ~/src/patentserver' C-m
  tmux send-keys -t patentserver 'rvm use 1.9.3-p286@patentserver' C-m
  tmux split-window -v -p 35 -t patentserver
  tmux split-window -v -p 50 -t patentserver
  tmux send-keys -t patentserver:0.1 'cd ~/src/patentserver' C-m
  tmux send-keys -t patentserver:0.2 'cd ~/src/patentserver' C-m
  tmux send-keys -t patentserver:0.1 'rvm use 1.9.3-p286@patentserver' C-m
  tmux send-keys -t patentserver:0.2 'rvm use 1.9.3-p286@patentserver' C-m
  tmux send-keys -t patentserver:0.2 'spork' C-m
  #tmux send-keys -t patentserver:0.1 # change to sass 'middleman' C-m
  tmux select-pane -t patentserver:0.0
#  tmux send-keys -t patentserver:0.0 'vi .' C-m

  tmux new-window -t patentserver -n server
  tmux send-keys -t patentserver:server 'cd ~/src/patentserver' C-m
  tmux send-keys -t patentserver:server 'rvm use ruby-1.9.3-p286@patentserver' C-m
  tmux send-keys -t patentserver:server 'rails s' C-m

  tmux new-window -t patentserver -n spec
  tmux send-keys -t patentserver:spec 'cd ~/src/patentserver' C-m
  tmux send-keys -t patentserver:spec 'rvm use ruby-1.9.3-p286@patentserver' C-m

  tmux new-window -t patentserver -n postgres
  tmux send-keys -t patentserver:postgres 'cd ~/src/patentserver' C-m
  tmux send-keys -t patentserver:postgres 'rvm use ruby-1.9.3-p286@patentserver' C-m

  tmux select-window -t patentserver:editing
  tmux select-pane -t patentserver:editing.0
fi
tmux attach-session -t patentserver
