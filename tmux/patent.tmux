#!/bin/sh

tmux has-session -t patent 2> /dev/null

if [ $? != 0 ]
then
  tmux new-session -s patent -n editing -d
  tmux send-keys -t patent 'cd ~/src/patent' C-m
  tmux send-keys -t patent 'rvm use 1.9.3-p286@patent' C-m
  tmux split-window -h -p 50 -t patent
  tmux split-window -v -p 35 -t patent
  tmux send-keys -t patent:0.1 'cd ~/src/patent' C-m
  tmux send-keys -t patent:0.1 'rvm use 1.9.3-p286@patent' C-m
  tmux send-keys -t patent:0.2 'cd ~/src/patent' C-m
  tmux send-keys -t patent:0.2 'rvm use 1.9.3-p286@patent' C-m
  tmux select-pane -t patent:0.0
fi
tmux attach-session -t patent
