#!/bin/sh

tmux has-session -t patentview 2> /dev/null

if [ $? != 0 ]
then
  tmux new-session -s patentview -n editing -d
  tmux send-keys -t patentview 'cd ~/src/patentview' C-m
  tmux send-keys -t patentview 'rvm use ruby-2.0.0-p0@patentview' C-m
  tmux split-window -v -p 35 -t patentview
  tmux split-window -v -p 50 -t patentview
  tmux send-keys -t patentview:0.1 'cd ~/src/patentview' C-m
  tmux send-keys -t patentview:0.2 'cd ~/src/patentview' C-m
  tmux send-keys -t patentview:0.1 'rvm use ruby-2.0.0-p0@patentview' C-m
  tmux send-keys -t patentview:0.2 'rvm use ruby-2.0.0-p0@patentview' C-m
  tmux send-keys -t patentview:0.2 'spork' C-m
  #tmux send-keys -t patentview:0.1 # change to sass 'middleman' C-m
  tmux select-pane -t patentview:0.0
#  tmux send-keys -t patentview:0.0 'vi .' C-m

  tmux new-window -t patentview -n view
  tmux send-keys -t patentview:view 'cd ~/src/patentview' C-m
  tmux send-keys -t patentview:view 'rvm use ruby-2.0.0-p0@patentview' C-m
  tmux send-keys -t patentview:view 'rails s' C-m

  tmux new-window -t patentview -n spec
  tmux send-keys -t patentview:spec 'cd ~/src/patentview' C-m
  tmux send-keys -t patentview:spec 'rvm use ruby-2.0.0-p0@patentview' C-m

  tmux new-window -t patentview -n postgres
  tmux send-keys -t patentview:postgres 'cd ~/src/patentview' C-m
  tmux send-keys -t patentview:postgres 'rvm use ruby-2.0.0-p0@patentview' C-m

  tmux select-window -t patentview:editing
  tmux select-pane -t patentview:editing.0
fi
tmux attach-session -t patentview
