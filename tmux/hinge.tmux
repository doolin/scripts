#!/bin/sh

tmux has-session -t hinge 2> /dev/null

if [ $? != 0 ]
then

  export dir="$HOME/hinge/src"
  export session="hinge"

  export basilisk="$dir/basilisk"
  tmux new-session -s $session -n editing -d
  tmux send-keys -t $session "cd $basilisk" C-m
  tmux split-window -h -p 50 -t $session
  tmux send-keys -t $session:0.1 "cd $basilisk" C-m
  tmux select-pane -t $session:0.0

  tmux new-window -t $session -n server
  tmux send-keys -t $session:server "cd $basilisk" C-m
  tmux send-keys -t $session:server 'rails s' C-m

  # sidekiq
  # coach tools

  export documentation="$dir/documentation"
  if [ -d $documentation ]; then
    tmux new-window -t $session -n documentation
    tmux split-window -h -p 50 -t documentation
    tmux send-keys -t $session:documentation.0 "cd $documentation" C-m
    tmux send-keys -t $session:documentation.1 "cd $documentation" C-m
    tmux send-keys -t $session:documentation 'mkdocs serve' C-m
  fi

  export turtle="$dir/turtle"
  if [ -d $turtle ]; then
    tmux new-window -t $session -n turtle
    tmux split-window -h -p 50 -t turtle
    tmux send-keys -t $session:turtle.0 "cd $turtle" C-m
    tmux send-keys -t $session:turtle.1 "cd $turtle" C-m
    tmux send-keys -t $session:turtle 'mkdocs serve' C-m
  fi

  export runbooks="$dir/runbooks"
  if [ -d $runbooks ]; then
    tmux new-window -t $session -n runbooks
    tmux split-window -h -p 50 -t runbooks
    tmux send-keys -t $session:runbooks.0 "cd $runbooks" C-m
    tmux send-keys -t $session:runbooks.1 "cd $runbooks" C-m
    tmux send-keys -t $session:runbooks 'mkdocs serve' C-m
  fi

  export ceros="$dir/ceros"
  if [ -d $ceros ]; then
    tmux new-window -t $session -n ceros
    tmux split-window -h -p 50 -t ceros
    tmux send-keys -t $session:ceros.0 "cd $ceros" C-m
    tmux send-keys -t $session:ceros.1 "cd $ceros" C-m
    tmux send-keys -t $session:ceros 'mkdocs serve' C-m
  fi

  # curriculum
  export curriculum="$dir/curriculum"

fi
tmux select-window -t $session:0
tmux attach-session -t $session
