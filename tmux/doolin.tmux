#!/bin/sh

export dir='~/src/dool.in'

tmux has-session -t doolin 2> /dev/null

if [ $? != 0 ]
then
  tmux new-session -s doolin -n daily -d
  tmux send-keys -t doolin "cd $dir" C-m
  tmux split-window -v -p 25 -t doolin
  tmux send-keys -t doolin:daily.1 "cd $dir" C-m

  tmux new-window -t doolin -n server
  tmux send-keys -t doolin:server "cd $dir" C-m
  tmux send-keys -t doolin:server 'middleman -p4569' C-m

  tmux new-window -t doolin -n sass
  tmux send-keys -t doolin:sass.0 "cd $dir" C-m
  tmux send-keys -t doolin:sass.0 'sass --watch source/stylesheets --style expanded' C-m

  tmux new-window -t doolin -n lib
  tmux send-keys -t doolin:lib "cd $dir/lib" C-m
  tmux split-window -h -p 250 -t doolin
  tmux send-keys -t doolin:lib.1 "cd $dir/lib" C-m

  tmux new-window -t doolin -n linear
  tmux send-keys -t doolin:linear "cd $dir/lib/linear-algebra" C-m
  tmux split-window -h -p 250 -t doolin
  tmux send-keys -t doolin:linear.1 "cd $dir/lib/linear-algebra" C-m

  tmux select-window -t doolin:daily
  tmux select-pane -t doolin:daily.0
fi
tmux attach-session -t doolin
