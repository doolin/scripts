#!/bin/sh

tmux has-session -t working 2> /dev/null

if [ $? != 0 ]
then
  tmux new-session -s working -n editing -d

  tmux send-keys -t working "cd ~/src/$1" C-m
  tmux send-keys -t working 'rvm use ruby-1.9.3-p385@working' C-m
  tmux split-window -v -p 25 -t working:editing
  tmux send-keys -t working:editing.1 "cd ~/src/$1" C-m
  tmux send-keys -t working:editing.1 'rvm use ruby-1.9.3-p385@working' C-m
  #tmux send-keys -t working:0.1 'middleman' C-m
  #tmux send-keys -t working:0.0 'vi .' C-m

  tmux new-window -t working -n server
  tmux send-keys -t working "cd ~/src/$1" C-m
  tmux send-keys -t working:server.0 'rvm use ruby-1.9.3-p385@working' C-m
  tmux send-keys -t working:server.0 'rails server' C-m

  tmux new-window -t working -n sass
  tmux send-keys -t working "cd ~/src/$1" C-m
  tmux send-keys -t working:sass.0 'rvm use ruby-1.9.3-p385@working' C-m
  tmux send-keys -t working:sass.0 'sass --watch app/assets/stylesheets --style expanded' C-m

  tmux select-pane -t working:editing.top
  tmux select-window -t working:editing
fi
tmux attach-session -t working
