#!/bin/sh

tmux has-session -t practice 2> /dev/null

set DIR=$HOME/Dropbox/deliberate-practice-for-programming/manuscript

if [ $? != 0 ]
then
  tmux new-session -s practice -n editing -d
  tmux send-keys -t practice 'cd $DIR' C-m
  tmux send-keys -t practice 'rvm use ruby-2.0.0-p0@practice' C-m
  tmux split-window -v -p 25 -t practice
  tmux send-keys -t practice:0.1 'cd ~/src/practice' C-m
  tmux send-keys -t practice:0.1 'rvm use ruby-2.0.0-p0@practice' C-m

  tmux new-window -t practice -n site
  tmux send-keys -t practice:site.0 'cd ~/src/practice/source' C-m
  tmux send-keys -t practice:site.0 'rvm use ruby-2.0.0-p0@practice' C-m


  tmux select-window -t practice:editing
  tmux select-pane -t practice:editing.0
fi
tmux attach-session -t practice
