#!/bin/sh

tmux has-session -t deck 2> /dev/null

if [ $? != 0 ]
then
  tmux new-session -s deck -n editing -d

  tmux send-keys -t deck "cd ~/src/$1" C-m
  #tmux send-keys -t deck 'rvm use ruby-1.9.3-p385@working' C-m
  tmux split-window -v -p 25 -t deck:editing
  tmux send-keys -t deck:editing.1 "cd ~/src/$1" C-m
  #tmux send-keys -t deck:editing.1 'rvm use ruby-1.9.3-p385@working' C-m
  #tmux send-keys -t deck:0.1 'middleman' C-m
  #tmux send-keys -t deck:0.0 'vi .' C-m

  tmux new-window -t deck -n server
  tmux send-keys -t deck "cd ~/src/$1" C-m
  #tmux send-keys -t deck:server.0 'rvm use ruby-1.9.3-p385@working' C-m
  tmux send-keys -t deck:server.0 "deck $1.md" C-m

  #tmux new-window -t deck -n sass
  #tmux send-keys -t deck "cd ~/src/$1" C-m
  #tmux send-keys -t deck:sass.0 'rvm use ruby-1.9.3-p385@deck' C-m
  #tmux send-keys -t deck:sass.0 'sass --watch app/assets/stylesheets --style expanded' C-m

  tmux select-pane -t deck:editing.top
  tmux select-window -t deck:editing
fi
tmux attach-session -t deck
