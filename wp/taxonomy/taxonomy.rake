#!/usr/bin/env ruby

# Run this file any of the following ways:
# * rake -f taxonomy.rake create
# * rake -f taxonomy.rake create['filename.yml']
# * rspec taxonomy.rake -f d --color
# * 

# http://pleac.sourceforge.net/pleac_ruby/fileaccess.html

=begin
TODO list:
* Use some regex magic to handle naming for classes, ids, etc.
* Reorganize the code in this file.
* Add command line handling options for choosing which .yml file to load
* Automatically name and emit the output file into the correct directory
  This needs to be done as a yml configuration option, which means the
  structure of this file needs to be rewritten as well.
* Write task documentation, especially for how command line options work
* Add methods for adding form fields of various types, fields specified in .yml
* Clean up how the class loading works.
=end

require 'rspec'
require 'yaml'
require 'rake' #otherwise rspec fails on "task" method
require 'titleize' #might implement anyway instead of fooling with gem

require '../maker.rb'


# TODO: Move these to configuration variables.
@project   = "wiaw"
@theme     = "exposed"
@deploydir = "/Applications/MAMP/htdocs/#{@project}/wp-content/themes/#{@theme}/lib"


task :default => ["deployment_test"]


task :deployment_test do
  sh "cp taxonomy_template.php #{@deploydir}"
end

class TaxonomyMaker < Maker

  attr_accessor :namespace,
                :taxonomy,
                :object_type,
                :general_name,
                :singular_name,
                :pluginname,
                :pluginuri,
                :plugindescription,
                :pluginauthor,
                :pluginauthoruri,
                :plugindisplay

  # http://blog.innovativethought.net/2007/07/25/making-configuration-files-with-yaml/
  # Try this as well: config["config"].each { |key, value| instance_variable_set("@#{key}", value) }
  # http://blog.innovativethought.net/2009/01/02/making-configuration-files-with-yaml-revised/
  def read_config(filename)
    config             = YAML.load_file(filename)
    @namespace         = config["config"]["namespace"]
    @taxonomy          = config["config"]["taxonomy"]
    @object_type       = config["config"]["object_type"]
    @general_name      = config["config"]["general_name"]
    @singular_name     = config["config"]["singular_name"]
  
    @pluginname        = config["plugin"]["pluginname"]
    @pluginuri         = config["plugin"]["uri"]
    @plugindescription = config["plugin"]["description"]
    @pluginauthor      = config["plugin"]["author"]
    @pluginauthoruri   = config["plugin"]["authoruri"]
    @plugindisplay     = config['plugin']['display']
  end

  def create_filename
    "#{@namespace}_#{@taxonomy}.php"
  end

  def create_class_name
    "#{@namespace}_#{@taxonomy}.css"
  end

  def create_filename
    add_underscores("#{@namespace}_#{@taxonomy}_taxonomy.php".downcase)
  end
  
  def create_taxonomy_term
    add_hyphens(@taxonomy.downcase)
  end
  
  def create_functionname
    add_underscores("#{@namespace}_create_#{@taxonomy}".downcase)
  end
end


=begin
task :create_outdir do 
  if !Dir.exists?(@testdir)
    Dir.mkdir(@testdir)
    Dir.mkdir(File.join(@testdir, "css"))
    Dir.mkdir(File.join(@testdir, "js"))
  end
end
=end

=begin
task :read_template do
  puts 'reading template...'
  sink = File.new("./out.php", "w")  # open file "path" for writing only
  File.open("./widget_template.php", "r").each { |line|
    #puts line
  }
end
=end


task :create, :filename do |t, file|
  @tm = TaxonomyMaker.new
  filename = file[:filename] ||= 'taxonomy.yml'
  @tm.read_config(filename)
  File.open('./taxonomy_template.php', 'r') do |f|   # open file for update
    @lines = f.readlines           # read into array of lines
    @lines.each do |it|            # modify lines
      it.gsub!(/NAMESPACE/,     @tm.namespace)
      it.gsub!(/TAXONOMY/,      @tm.taxonomy)
      it.gsub!(/OBJECT_TYPE/,   @tm.object_type)
      it.gsub!(/GENERAL_NAME/,  @tm.general_name)
      it.gsub!(/SINGULAR_NAME/, @tm.singular_name)
      #it.gsub!(/FUNCTIONNAME/,  @tm.create_functionname)
      
      it.gsub!(/PLUGINNAME/,    @tm.pluginname)
      it.gsub!(/PLUGINURI/,     @tm.pluginuri)
      it.gsub!(/PLUGINDESCRIPTION/, @tm.plugindescription)
      it.gsub!(/PLUGINAUTHOR/,  @tm.pluginauthor)
      it.gsub!(/AUTHORURI/,     @tm.pluginauthoruri)
      it.gsub!(/DISPLAYCODE/,   @tm.plugindisplay)
    end
  end
  #filename = @tm.create_filename #"#{@tm.namespace}-#{@tm.taxonomy}-taxonomy.php"
  filename = "#{@tm.namespace}-#{@tm.taxonomy}-taxonomy.php"
  f = File.open(filename, 'w') 
  @lines.each { |l| f << l }
  f.close
  sh "cp #{filename} #{@deploydir}"
end

describe TaxonomyMaker do

  before(:each) do
    @tm = TaxonomyMaker.new
    @tm.read_config('taxonomy.yml') #dumb, need to read once somehow...
  end

  it "should be a new TaxonomyMaker" do
    expect {
      @tm.should_not be_nil
    }
  end

  # This will be improved later...
  it "should downcase the widget's name" do
    @tm.lowercase('Taxonomy Name').should eq('taxonomy name')
  end

  it "replaces spaces with underscores" do
    @tm.add_underscores('namespace taxonomy name').should eq('namespace_taxonomy_name')
  end

  it "replaces spaces with hyphens" do
    @tm.add_hyphens('namespace taxonomy name').should eq('namespace-taxonomy-name')
  end

  it "creates the php file name" do
    @tm.create_filename.should eq('namespace_taxonomy_foo_taxonomy.php')
  end

  it "creates the taxonomy term" do
    @tm.create_taxonomy_term.should eq('taxonomy-foo')
  end

  it "creates the function name" do
    @tm.create_functionname.should eq('namespace_create_taxonomy_foo')
  end


  it "creates the css class file name"

  it "creates the element class name"

  it "creates the element id"

  it "creates the plugin name"

  it "creates the plugin description"

  it "creates the plugin uri"

  it "creates the plugin author name"

  it "creates the plugin author uri"

  it "does whatever display code is supposed to do"

  it "throws the appropriate exception for DNE files"

end
