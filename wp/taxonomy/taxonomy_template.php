<?php
/** From:
 * Codex: http://codex.wordpress.org/Taxonomies
 * Codex: http://codex.wordpress.org/Function_Reference/register_taxonomy
 */
function FUNCTIONNAME() {

 $labels = array(
    'name'                       => _x( 'GENERAL_NAME', 'taxonomy general name' ),
    'singular_name'              => _x( 'SINGULAR_NAME', 'taxonomy singular name' ),
    'search_items'               => __( 'Search GENERAL_NAME', 'NAMESPACE' ),
    'popular_items'              => __( 'Popular GENERAL_NAME', 'NAMESPACE' ), //- the popular items text. Default is __( 'Popular Tags' ) or null
    'all_items'                  => __( 'All GENERAL_NAME', 'NAMESPACE' ), //- the all items text. Default is __( 'All Tags' ) or __( 'All Categories' )
    'parent_item'                => __( 'Parent GENERAL_NAME', 'NAMESPACE' ), //- the parent item text. This string is not used on non-hierarchical taxonomies such as post tags. Default is null or __( 'Parent Category' )
    'parent_item_colon'          => __( 'Parent SINGULAR_NAME:', 'NAMESPACE' ),
    'edit_item'                  => __( 'Edit SINGULAR_NAME', 'NAMESPACE' ),
    'update_item'                => __( 'Update SINGULAR_NAME', 'NAMESPACE' ),
    'add_new_item'               => __( 'Add New SINGULAR_NAME', 'NAMESPACE' ),
    'new_item_name'              => __( 'New SINGULAR_NAME Name', 'NAMESPACE' ),
    'separate_items_with_commas' => '', //- the separate item with commas text used in the taxonomy meta box. This string isn't used on hierarchical taxonomies. Default is __( 'Separate tags with commas' ), or null
    'add_or_remove_items'        => '', //- the add or remove items text and used in the meta box when JavaScript is disabled. This string isn't used on hierarchical taxonomies. Default is __( 'Add or remove tags' ) or null
    'choose_from_most_used'      => '', //- the choose from most used text used in the taxonomy meta box. This string isn't used on hierarchical taxonomies. Default is __( 'Choose from the most used tags' ) or null
    'menu_name'                  => 'GENERAL_NAME'  //- the menu name text. This string is the name to give menu items. Defaults to value of name
  );  

  $taxonomy    = 'TAXONOMY';
  $object_type = array('OBJECT_TYPE');
  $args        = array(
                 'labels'                => $labels,
                 'public'                => true,
                 'show_in_nav_menus'     => true,
                 'show_ui'               => true,
                 'show_taglcoud'         => true,
                 'hierarchical'          => true,
                 'update_count_callback' => false,
                 'query_var'             => true,
                 //'rewrite'               => array( 'slug' => 'genre' )
                 //'capabilities'          => ''
                 );

  register_taxonomy($taxonomy, $object_type, $args);
}
add_action( 'init', 'FUNCTIONNAME' );

?>