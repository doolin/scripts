class related_links_metabox {

  var $keys;

  function related_links_metabox() {
    $this->keys = array('related-links');
    add_action('add_meta_boxes', array($this,'init'));
    add_action('save_post',array($this,'save_metabox'));
  }


  function init() {

    add_meta_box(
     'related_links',
  __('Related Links', 'heyday'),
     array($this, 'related_links_data_metabox'),
     'book',
     'side');

    add_meta_box(
     'related_links',
  __('Related Links', 'heyday'),
     array($this, 'related_links_data_metabox'),
     'person',
     'side');
  }


  function related_links_data_metabox() {

    wp_nonce_field(plugin_basename(__FILE__), 'heyday_links_noncename');

    $single = true;
    global $post;
    $post_id = $post->ID;

    echo '<p>';
    $value = 'related-links';
    $fieldname = tl_utilities::make_field_name($value);
    $id = tl_utilities::make_id_name($value);
    $namevalue = get_post_meta($post_id, $fieldname, $single);
    echo '<label for="' . $id . '">List related links here</label><br />';
    echo '<textarea id="related-links-text" cols="35" name="' . $fieldname . '" rows="7">';
    echo $namevalue;
    echo '</textarea>';
    echo '</p>';
  }


  function cannot_save_postmeta_for($postid) {

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { return true; }

    if (!wp_verify_nonce($_POST['heyday_links_noncename'], plugin_basename(__FILE__))) { return true; }
    
    if (isset( $_POST['post_type']) && ('book' == $_POST['post_type'] || 'person' == $_POST['post_type'])) {
      if (!current_user_can('edit_post', $postid)) return true;
    } else {
      return true;
    }
    return false;
  }


  function save_postmeta($post_id, $keys) {

    foreach ($keys as $key => $value) {
      $fieldname = tl_utilities::make_field_name($value);
      $fieldvalue = isset($_POST[$fieldname]) ? $_POST[$fieldname] : null;
      if ($fieldvalue){
        update_post_meta($post_id, $fieldname, $fieldvalue);
      } else {
        delete_post_meta($post_id, $fieldname);
      }
    } 
  }


  function save_metabox($post_id) {

    if ($this->cannot_save_postmeta_for($post_id)) {return;}
    $this->save_postmeta($post_id, $this->keys);
  }

}

