<?php
/**
 * Plugin name: PLUGINNAME
 * Plugin URI: PLUGINURI
 * Description: PLUGINDESCRIPTION
 * Version: 0.1
 * Author: PLUGINAUTHOR
 * Author URI: AUTHORURI
 */

// Codex: http://codex.wordpress.org/Widgets_API#Developing_Widgets
if (!class_exists('NAMESPACE_CLASSNAME_widget')) {

  class NAMESPACE_CLASSNAME_widget extends WP_Widget {


  /* TODO: Adjust class and id names as appropriate. */
    var $css_class        = 'NAMESPACE-CLASSNAME-widget';
    var $base_id          = 'NAMESPACE-CLASSNAME-widget';

    var $description      = 'WIDGETDESCRIPTION';
    var $name             = 'WIDGETNAME';
    var $default_title    = 'TITLE';
    var $default_demotext = 'DEMOTEXT';


    function NAMESPACE_CLASSNAME_widget() {

      $widget_ops = array('classname' => $this->css_class, 
                          'description' => $this->description);
      $this->WP_Widget($this->base_id, $this->name, $widget_ops);
      add_action('display-related-links', array($this, 'display'));
    }


    /* This is the code that gets displayed on the UI side,
     * what readers see.
     */
    function widget($args, $instance) {

      extract($args, EXTR_SKIP);
      echo $before_widget;
      $title = (empty($instance['title'])) ? $this->default_title : apply_filters('widget_title', $instance['title']); 
      $demotext = (empty($instance['demotext'])) ? $this->default_demotext : $instance['demotext']; 
      echo $before_title . $title . $after_title; 
      do_action('display-related-links');
      echo $after_widget;
    }


    function display() {
      
      $display = <<<EOD
      DISPLAYCODE
EOD;
      return $display;
    }

    function update($new_instance, $old_instance) {
      
      $instance = $old_instance;
      $instance['title'] = strip_tags($new_instance['title']);
      $instance['demotext'] = strip_tags($new_instance['demotext']);
      return $instance;
    }


    /* Back end, the interface shown in Appearance -> Widgets
     * administration interface.
     */
    function form($instance) {

      $instance = wp_parse_args( (array) $instance, array( 'title' => '', 'demotext' => '' ) );

      $title    = esc_attr(strip_tags($instance['title']));
      $title_id = $this->get_field_id('title');
      $title_name = $this->get_field_name('title');

      $demotext = esc_attr(strip_tags($instance['demotext']));
 
      echo "<p>";
      echo $this->text_input_instance($instance, 'title', 'Title');
      echo $this->text_input_instance($instance, 'demotext', 'Demo text');
      echo "</p>";
    }


    // Clean everything up...
    function text_input_instance($instance, $key, $label) {

      $value    = esc_attr(strip_tags($instance[$key]));    
      $id = $this->get_field_id($key);
      $name = $this->get_field_name($key);
      return $this->text_input($id, $name, $value, $label);
    }


    // This is where overloading would be real handy...
    function text_input($id, $name, $value, $label) {

      $input = <<<EOI
      <label for="$id">$label: 
        <input class="widefat" id="$id" name="$name" type="text" value="$value" />
      </label>
EOI;
      return $input;
    }

  } // Close class definition...



  function NAMESPACE_CLASSNAME_widget_init() {
    // http://codex.wordpress.org/Function_Reference/register_widget
    register_widget('NAMESPACE_CLASSNAME_widget');
  }
  add_action('widgets_init', 'NAMESPACE_CLASSNAME_widget_init');

} // class_exists()...


?>
