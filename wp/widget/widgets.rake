#!/usr/bin/env ruby

# Run this file any of the following ways:
# * rake -f widgets.rake create
# * rake -f widgets.rake create['filename.yml']
# * rspec widgets.rake -f d --color
# * 

require 'rspec'
require 'yaml'
require 'rake' #otherwise rspec fails on "task" method
require 'titleize' #might implement anyway instead of fooling with gem

require '../maker.rb'

# directory "widget"
# http://pleac.sourceforge.net/pleac_ruby/fileaccess.html

=begin

TODO list:

* Compare this rake file with the file in kerastase; merge necessary
* Use some regex magic to handle naming for classes, ids, etc.
* Reorganize the code in this file.
* Add command line handling options for choosing which .yml file to load
* Automatically name and emit the output file into the correct directory
* Write task documentation, especially for how command line options work
* Add methods for adding form fields of various types, fields specified in .yml
* Clean up how the class loading works.
=end

# Rakefile

# TODO: Move these to configuration variables.
@project   = "ktsf"
@theme     = "ktsf"
@deploydir = "/Applications/MAMP/htdocs/#{@project}/wp-content/themes/#{@theme}/lib"

class WidgetMaker < Maker
  
  attr_accessor :namespace,
                :css_file,
                :css_class,
                :demotext,
                :base_id,
                :basename,
                :classname,
                :widgetdescription,
                :widgetname,
                :default_title,
                :default_demotext,
                :pluginname,
                :pluginuri,
                :plugindescription,
                :pluginauthor,
                :pluginauthoruri,
                :plugindisplay

# string.each(" ") {|word| word.capitalize }
# or titleize if the gem is installed

  def read_config(filename)
    # TODO: Handle exception when filename doesn't exist.
    # TODO: RSpec the exception handling
    config             = YAML.load_file(filename)
    @namespace         = config["config"]["namespace"]
    @demotext          = config["config"]["default_demotext"]
    @classname         = config["config"]["classname"]
    @widgetdescription = config["config"]["description"]
    @widgetname        = config["config"]["widgetname"]
    @default_title     = config["config"]["default_title"]
    @default_demotext  = config["config"]["default_demotext"]
    #@css_file          = config["config"]["css_file"]
    #@css_class         = config["config"]["css_class"]
    #@base_id           = config["config"]["base_id"]
    #@basename          = config["config"]["basename"]

    @pluginname        = config["plugin"]["pluginname"]
    @pluginuri         = config["plugin"]["uri"]
    @plugindescription = config["plugin"]["description"]
    @pluginauthor      = config["plugin"]["author"]
    @pluginauthoruri   = config["plugin"]["authoruri"]
    @plugindisplay     = config['plugin']['display']
  end

  # This is stupid code, but it's being used to build out 
  # the spec testing framework. Will be deleted later.
  # Meantime, these are explicit until the actual naming
  # schema is finalized. Then these regular expressions
  # can be moved in line, or done however the accepted
  # idiom dictates.
  def lowercase(term)
    term.downcase
  end

  def create_filename
    add_underscores("#{@namespace} #{@classname} widget.php".downcase)
  end

  def write_file(lines)
    filename = create_filename #"#{@cptm.namespace}_#{@cptm.post_type}_type.php"
    f = File.open(filename, 'w') 
    lines.each {|l| f << l }
    f.close
    return filename
  end
end


@testdir = "testdir"

# http://blog.innovativethought.net/2007/07/25/making-configuration-files-with-yaml/
# Try this as well: config["config"].each { |key, value| instance_variable_set("@#{key}", value) }
# http://blog.innovativethought.net/2009/01/02/making-configuration-files-with-yaml-revised/

task :default => ["testem"]

task :testem => [:read_template] do 
  puts "test..."
  read_config
end

task :create_outdir do 
  if !Dir.exists?(@testdir)
    Dir.mkdir(@testdir)
    Dir.mkdir(File.join(@testdir, "css"))
    Dir.mkdir(File.join(@testdir, "js"))
  end
end


task :read_template do

  puts 'reading template...'
  sink = File.new("./out.php", "w")  # open file "path" for writing only
  File.open("./widget_template.php", "r").each { |line|
    #puts line
  }
end


desc "Generate code for a widget with rake -f widgets.rake create"
task :create, :filename do |t, file|
  @wm = WidgetMaker.new
  filename = file[:filename] ||= 'widget.yml'
  @wm.read_config(filename)
  #puts @wm.instance_variables
  #puts @wm.pluginname
  #puts @wm.css_file

  File.open('./widget_template.php', 'r') do |f|   # open file for update
    lines = f.readlines           # read into array of lines
    lines.each do |it|            # modify lines
      it.gsub!(/NAMESPACE/,         @wm.namespace)
      it.gsub!(/CLASSNAME/,         @wm.classname)
      it.gsub!(/WIDGETNAME/,        @wm.widgetname)
      # http://www.ruby-doc.org/core/classes/String.html#M001186
      # Stick a block on the end of these.
      it.gsub!(/WIDGETDESCRIPTION/, @wm.widgetdescription)
      it.gsub!(/TITLE/,             @wm.default_title)
      it.gsub!(/DEMOTEXT/,          @wm.demotext)
      
      it.gsub!(/PLUGINNAME/,        @wm.pluginname)
      it.gsub!(/PLUGINURI/,         @wm.pluginuri)
      it.gsub!(/PLUGINDESCRIPTION/, @wm.plugindescription)
      it.gsub!(/PLUGINAUTHOR/,      @wm.pluginauthor)
      it.gsub!(/AUTHORURI/,         @wm.pluginauthoruri)
      it.gsub!(/DISPLAYCODE/,       @wm.plugindisplay)
      
      # Add a gsub and block for text input fields, etc.
      # puts it
    end
    outfile = @wm.write_file(lines)
    sh "cp #{outfile} #{@deploydir}"
  end
end

describe WidgetMaker do
  
  before(:each) do
    @wm = WidgetMaker.new
    @wm.read_config('widget.yml') #dumb, need to read once somehow...
  end

  it "should be a new WidgetMaker" do
    expect {
      @wm.should_not be_nil
    }
  end

  it "creates the php file name" do
    @wm.create_filename.should eq('namespace_classname_widget.php')
  end

=begin
  it "creates the taxonomy term" do
    @tm.create_taxonomy_term.should eq('taxonomy-foo')
  end

  it "creates the function name" do
    @tm.create_functionname.should eq('namespace_create_taxonomy_foo')
  end
=end

  xit "creates the css class file name" do
    @wm.foo.should eq('')
  end

  xit "creates the element class name" do
    @wm.foo.should eq('')
  end

  xit "creates the element id" do
    @wm.foo.should eq('')
  end

  it "creates the plugin name" do
    @wm.pluginname.should eq('NAMESPACE NAME Widget')
  end

  it "creates the plugin description" do
    @wm.plugindescription.should eq('Plugin file for NAMESPACE NAME Widget.')
  end

  xit "creates the plugin uri" do
    @wm.foo.should eq('')
  end

  xit "creates the plugin author name" do
    @wm.foo.should eq('')
  end

  xit "creates the plugin author uri" do
    @wm.foo.should eq('')
  end

  xit "does whatever display code is supposed to do" do
    @wm.foo.should eq('')
  end
  
  xit "throws the appropriate exception for DNE files" do
    @wm.foo.should eq('')
  end

end
