<?php

function ktsf_create_event_type() {

  $supports = array(
  'thumbnail', 
  'custom-fields', 
  'excerpt', 
  'title', 
  'editor', 
  'revisions');
  
  $labels = array(
    'name'               => __('Events' ),
    'singular_name'      => __('Event' ),
    'add_new'            => __('Add Event'),
    'add_new_item'       => __('Add New Event'),
    'edit'               => __('Edit'),
    'edit_item'          => __('Edit Event'),
    'new_item'           => __('New Event'),
    'view'               => __('View Event'),
    'view_item'          => __('View Event'),
    'search_items'       => __('Search Events'),
    'not_found'          => __('No Events Found'),
    'not_found_in_trash' => __('No Events Found in Trash'),
    'parent'             => __('Parent Event')
  );
  
  register_post_type( 'event',
  array(
    'labels'        => $labels,
    'public'        => true,
    'has_archive'   => true,
    'description'   => "Events of interest to the KTSF community",
    'menu_position' => 5,
    'taxonomies'    => array('post_tags', 'events', 'imprint'),
    'supports'      => $supports 
  )
  );
}
add_action( 'init', 'ktsf_create_event_type' );

?>
