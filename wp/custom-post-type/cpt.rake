#!/usr/bin/env ruby

# Run this file any of the following ways:
# * rake -f cpt.rake create
# * rake -f cpt.rake create['filename.yml']
# * rspec cpt.rake -f d --color
# * 

require 'rspec'
require 'yaml'
require 'rake' #otherwise rspec fails on "task" method
require 'titleize' #might implement anyway instead of fooling with gem

require '../maker.rb'


# http://pleac.sourceforge.net/pleac_ruby/fileaccess.html

=begin
TODO list:
* Use some regex magic to handle naming for classes, ids, etc.
* Reorganize the code in this file.
* Add command line handling options for choosing which .yml file to load
* Automatically name and emit the output file into the correct directory
  This needs to be done as a yml configuration option, which means the
  structure of this file needs to be rewritten as well.
* Write task documentation, especially for how command line options work
* Add methods for adding form fields of various types, fields specified in .yml
* Clean up how the class loading works.
=end


# TODO: Move these to configuration variables.
@project   = "wiaw"
@theme     = "exposed"
@deploydir = "/Applications/MAMP/htdocs/#{@project}/wp-content/themes/#{@theme}/lib"


task :default => ["deployment_test"]


task :deployment_test do
  sh "cp cpt_template.php #{@deploydir}"
end

class CustomPostTypeMaker < Maker

  attr_accessor :namespace,
                :post_type,
                :general_name,
                :singular_name,
                :description,
                :pluginname,
                :pluginuri,
                :plugindescription,
                :pluginauthor,
                :pluginauthoruri,
                :plugindisplay

  # http://blog.innovativethought.net/2007/07/25/making-configuration-files-with-yaml/
  # Try this as well: config["config"].each { |key, value| instance_variable_set("@#{key}", value) }
  # http://blog.innovativethought.net/2009/01/02/making-configuration-files-with-yaml-revised/
  def read_config(filename)
    config             = YAML.load_file(filename)
    @namespace         = config["config"]["namespace"]
    @post_type         = config["config"]["post_type"]
    @general_name      = config["config"]["general_name"]
    @singular_name     = config["config"]["singular_name"]
    @description       = config["config"]["description"]

    @pluginname        = config["plugin"]["pluginname"]
    @pluginuri         = config["plugin"]["uri"]
    @plugindescription = config["plugin"]["description"]
    @pluginauthor      = config["plugin"]["author"]
    @pluginauthoruri   = config["plugin"]["authoruri"]
    @plugindisplay     = config['plugin']['display']
  end

  # This is stupid code, but it's being used to build out
  # the spec testing framework. Will be deleted later.
  # Meantime, these are explicit until the actual naming
  # schema is finalized. Then these regular expressions
  # can be moved in line, or done however the accepted
  # idiom dictates.
  def lowercase(term)
    term.downcase
  end

  # From http://www.ruby-forum.com/topic/155944
  def add_underscores(term)
    term.gsub!(' ','_')
  end

  def add_hyphens(term)
    term.gsub!(' ','-')
  end

  def create_filename
    "#{@namespace}_#{@post_type}_type.php"
  end

  def create_class_name
    "#{@namespace}_#{@post_type}.css"
  end
  
  def write_file(lines)
    filename = "#{@cptm.namespace}_#{@cptm.post_type}_type.php"
    f = File.open(filename, 'w') 
    lines.each {|l| f << l }
    f.close
    sh "cp #{filename} #{@deploydir}"
  end
end

=begin
task :create_outdir do 
  if !Dir.exists?(@testdir)
    Dir.mkdir(@testdir)
    Dir.mkdir(File.join(@testdir, "css"))
    Dir.mkdir(File.join(@testdir, "js"))
  end
end
=end

=begin
task :read_template do
  puts 'reading template...'
  sink = File.new("./out.php", "w")  # open file "path" for writing only
  File.open("./widget_template.php", "r").each { |line|
    #puts line
  }
end
=end

desc "Create task..."
task :create, :filename do |t, file|
  @cptm = CustomPostTypeMaker.new
  filename = file[:filename] ||= 'widget.yml'
  @cptm.read_config(filename)
  File.open('./cpt_template.php', 'r') do |f|   # open file for update
    @lines = f.readlines           # read into array of lines
    @lines.each do |it|            # modify lines
      it.gsub!(/NAMESPACE/,     @cptm.namespace)
      it.gsub!(/POST_TYPE/,     @cptm.post_type)
      it.gsub!(/GENERAL_NAME/,  @cptm.general_name)
      it.gsub!(/SINGULAR_NAME/, @cptm.singular_name)
      #it.gsub!(/DESCRIPTION/,   @cptm.description)
      
      #it.gsub!(/PLUGINNAME/,    @cptm.pluginname)
      it.gsub!(/PLUGINURI/,     @cptm.pluginuri)
      it.gsub!(/PLUGINDESCRIPTION/, @cptm.plugindescription)
      it.gsub!(/PLUGINAUTHOR/,  @cptm.pluginauthor)
      it.gsub!(/AUTHORURI/,     @cptm.pluginauthoruri)
      it.gsub!(/DISPLAYCODE/,   @cptm.plugindisplay)
    end
  end
  filename = "#{@cptm.namespace}_#{@cptm.post_type}_type.php"
  f = File.open(filename, 'w') 
  @lines.each {|l| f << l }
  f.close
  sh "cp #{filename} #{@deploydir}"
  #puts @lines
end


describe CustomPostTypeMaker do
  
  before(:each) do
    @cptm = CustomPostTypeMaker.new
    @cptm.read_config('cpt.yml') #dumb, need to read once somehow...
  end

  it "should be a new CustomPostTypeMaker" do
    expect {
      @cptm.should_not be_nil
    }
  end

  # This will be improved later...
  it "should downcase the custom post type's name" do
    @cptm.lowercase('Post Name').should eq('post name')
  end

  it "replaces spaces with underscores" do
    @cptm.add_underscores('post name').should eq('post_name')
  end

  it "replaces spaces with hyphens" do
    @cptm.add_hyphens('post name').should eq('post-name')
  end

  it "creates the output file name" do
    @cptm.create_filename.should eq('NAMESPACE_POST_TYPE_type.php')
  end

  it "creates the css class file name" do
    @cptm.create_class_name.should eq('NAMESPACE_POST_TYPE.css')
  end

  it "creates the element class name"

  it "creates the element id"

  it "creates the plugin name"

  it "creates the plugin description"

  it "creates the plugin uri"

  it "creates the plugin author name"

  it "creates the plugin author uri"

  it "does whatever display code is supposed to do"
  
  it "throws the appropriate exception for DNE files"

end
