<?php

/**
 * @url http://codex.wordpress.org/Function_Reference/register_post_type
 *
 * See also:
 * http://lists.automattic.com/pipermail/wp-testers/2010-May/012980.html
 */
function NAMESPACE_create_POST_TYPE_type() {

  $supports = array(
    'thumbnail',
    'custom-fields',
    'excerpt',
    'title',
    'editor',
    'revisions');

  $labels = array(
    'name'               => __('GENERAL_NAME' ),
    'singular_name'      => __('SINGULAR_NAME' ),
    'add_new'            => __('Add SINGULAR_NAME'),
    'add_new_item'       => __('Add New SINGULAR_NAME'),
    'edit'               => __('Edit'),
    'edit_item'          => __('Edit SINGULAR_NAME'),
    'new_item'           => __('New SINGULAR_NAME'),
    'view'               => __('View SINGULAR_NAME'),
    'view_item'          => __('View SINGULAR_NAME'),
    'search_items'       => __('Search GENERAL_NAME'),
    'not_found'          => __('No GENERAL_NAME Found'),
    'not_found_in_trash' => __('No GENERAL_NAME Found in Trash'),
    'parent'             => __('Parent SINGULAR_NAME')
  );

  $args = array(
    'labels'        => $labels,
    'public'        => true,
    'has_archive'   => true,
    'description'   => "DESCRIPTION",
    'show_ui'       => true,
    'show_in_menu'  => true,
    'show_in_nav_menus' => true,
    'capability_type' => 'post',
    'menu_position' => null,
    'taxonomies'    => array('post_tags', 'events'),
    'supports'      => $supports,
    //'has_archive'   => false,
    'rewrite' => array('slug' => 'POST_TYPE')
  );

  register_post_type( 'POST_TYPE', $args);
}
add_action( 'init', 'NAMESPACE_create_POST_TYPE_type' );

/**
 * Everything past here needs to be tested.
 */
//add filter to ensure the text custom post type information, is displayed when user updates an entry
function NAMESPACE_book_updated_messages( $messages ) {
  global $post, $post_ID;

  $messages['book'] = array(
    0 => '', // Unused. Messages start at index 1.
    1 => sprintf( __('Book updated. <a href="%s">View book</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Book updated.'),
    /* translators: %s: date and time of the revision */
    5 => isset($_GET['revision']) ? sprintf( __('Book restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Book published. <a href="%s">View book</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Book saved.'),
    8 => sprintf( __('Book submitted. <a target="_blank" href="%s">Preview book</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Book scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview book</a>'),
      // translators: Publish box date format, see http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Book draft updated. <a target="_blank" href="%s">Preview book</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );

  return $messages;
}
//add_filter('post_updated_messages', 'NAMESPACE_book_updated_messages');


function NAMESPACE_add_help_text($contextual_help, $screen_id, $screen) { 
  //$contextual_help .= var_dump($screen); // use this to help determine $screen->id
  if ('book' == $screen->id ) {
    $contextual_help =
      '<p>' . __('Things to remember when adding or editing a book:') . '</p>' .
      '<ul>' .
      '<li>' . __('Specify the correct genre such as Mystery, or Historic.') . '</li>' .
      '<li>' . __('Specify the correct writer of the book.  Remember that the Author module refers to you, the author of this book review.') . '</li>' .
      '</ul>' .
      '<p>' . __('If you want to schedule the book review to be published in the future:') . '</p>' .
      '<ul>' .
      '<li>' . __('Under the Publish module, click on the Edit link next to Publish.') . '</li>' .
      '<li>' . __('Change the date to the date to actual publish this article, then click on Ok.') . '</li>' .
      '</ul>' .
      '<p><strong>' . __('For more information:') . '</strong></p>' .
      '<p>' . __('<a href="http://codex.wordpress.org/Posts_Edit_SubPanel" target="_blank">Edit Posts Documentation</a>') . '</p>' .
      '<p>' . __('<a href="http://wordpress.org/support/" target="_blank">Support Forums</a>') . '</p>' ;
  } elseif ( 'edit-book' == $screen->id ) {
    $contextual_help = 
      '<p>' . __('This is the help screen displaying the table of books blah blah blah.') . '</p>' ;
  }
  return $contextual_help;
}
//add_action( 'contextual_help', 'NAMESPACE_add_help_text', 10, 3 );

/*
function my_rewrite_flush() {
  my_cpt_init();
  flush_rewrite_rules();
}
register_activation_hook(__FILE__, 'my_rewrite_flush');
*/
?>
