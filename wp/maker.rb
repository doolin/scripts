
require 'rspec'
require 'titleize' #might implement anyway instead of fooling with gem


class Maker

# Pluralization:
# http://stackoverflow.com/questions/2446156/pluralize-function-in-ruby-not-rails

  # This is stupid code, but it's being used to build out
  # the spec testing framework. Will be deleted later.
  # Meantime, these are explicit until the actual naming
  # schema is finalized. Then these regular expressions
  # can be moved in line, or done however the accepted
  # idiom dictates.
  def lowercase(term)
    term.downcase
  end

  # From http://www.ruby-forum.com/topic/155944
  # no gsubs returns nil
  def add_underscores(term)
    term.gsub!(' ','_')
  end

  def add_hyphens(term)
    term.gsub!(' ','-')
  end

=begin
  def create_class_name
    "#{@namespace}_#{@taxonomy}.css"
  end

#  def create_filename
#    add_underscores("#{@namespace}_#{@taxonomy}_taxonomy.php".downcase)
#  end

  def create_functionname
    add_underscores("#{@namespace}_create_#{@taxonomy}".downcase)
  end
=end

end



describe Maker do

  before(:each) do
    @tm = Maker.new
    #@tm.read_config('taxonomy.yml') #dumb, need to read once somehow...
  end

  it "should be a new Maker" do
    expect {
      @tm.should_not be_nil
    }
  end

  # This will be improved later...
  it "should downcase the widget's name" do
    @tm.lowercase('Maker Name').should eq('maker name')
  end

  it "replaces spaces with underscores" do
    @tm.add_underscores('namespace maker name').should eq('namespace_maker_name')
  end

  it "replaces spaces with hyphens" do
    @tm.add_hyphens('namespace maker name').should eq('namespace-maker-name')
  end

=begin
  it "creates the php file name" do
    @tm.create_filename.should eq('namespace_taxonomy_foo_taxonomy.php')
  end

  it "creates the taxonomy term" do
    @tm.create_taxonomy_term.should eq('taxonomy-foo')
  end

  it "creates the function name" do
    @tm.create_functionname.should eq('namespace_create_taxonomy_foo')
  end


  it "creates the css class file name"

  it "creates the element class name"

  it "creates the element id"

  it "creates the plugin name"

  it "creates the plugin description"

  it "creates the plugin uri"

  it "creates the plugin author name"

  it "creates the plugin author uri"

  it "does whatever display code is supposed to do"

  it "throws the appropriate exception for DNE files"
=end
end
