
=begin

A WordPress component consists of:

 * Widget, either dynamic or static inclusion of content
 * Content generation mechanism, including data entry, which might mean one or more metaboxes.
 * Content delivery, including formatting.
 * Custom post type.
 * Taxonomy associated with custom post type. 
 * Associated code such as Javascript helpers, etc.

=end



