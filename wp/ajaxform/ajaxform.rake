
# directory "widget"

# http://pleac.sourceforge.net/pleac_ruby/fileaccess.html

=begin

TODO list:

* Compare this rake file with the file in kerastase; merge necessary
* Use some regex magic to handle naming for classes, ids, etc.
* Reorganize the code in this file.
* Add command line handling options for choosing which .yml file to load
* Automatically name and emit the output file into the correct directory
* Write task documentation, especially for how command line options work
* Add methods for adding form fields of various types, fields specified in .yml
* Clean up how the class loading works.


# Rakefile

require 'yaml'

@testdir = "testdir"


# http://blog.innovativethought.net/2007/07/25/making-configuration-files-with-yaml/
# Try this as well: config["config"].each { |key, value| instance_variable_set("@#{key}", value) }
# http://blog.innovativethought.net/2009/01/02/making-configuration-files-with-yaml-revised/
def read_config
  config             = YAML.load_file("kerastase.yml")
  @css_file          = config["config"]["css_file"]
  @css_class         = config["config"]["css_class"]
  @demotext          = config["config"]["default_demotext"]
  @base_id           = config["config"]["base_id"]
  @name              = config["config"]["name"]
  @basename          = config["config"]["basename"]
  @classname         = config["config"]["classname"]
  @widgetdescription = config["config"]["description"]
  @widgetname        = config["config"]["widgetname"]
  @default_title     = config["config"]["default_title"]
  @default_demotext  = config["config"]["default_demotext"]
  
  @pluginuri         = config["plugin"]["uri"]
  @plugindescription = config["plugin"]["description"]
  @pluginauthor      = config["plugin"]["author"]
  @pluginauthoruri   = config["plugin"]["authoruri"]
  @plugindisplay     = config['plugin']['display']
  
  puts 'CSS file: ', @css_file, "\n"
end


task :default => ["testem"]

task :testem => [:read_template] do 
  puts "test..."
  read_config
end

task :create_outdir do 
  if !Dir.exists?(@testdir)
    Dir.mkdir(@testdir)
    Dir.mkdir(File.join(@testdir, "css"))
    Dir.mkdir(File.join(@testdir, "js"))
  end
end


task :read_template do

  puts 'reading template...'
  sink = File.new("./out.php", "w")  # open file "path" for writing only
  File.open("./widget_template.php", "r").each { |line|
    #puts line
  }
end



task :create do
  read_config
  File.open('./widget_template.php', 'r') do |f|   # open file for update
    lines = f.readlines           # read into array of lines
    lines.each do |it|            # modify lines
      #it.gsub!(/Demo Plugin/, 'Foo Bar')
      #it.gsub!(/demo_plugin/, 'foo_bar')
      it.gsub!(/PLUGINNAME/, @name)
      it.gsub!(/BASENAME/, @basename)
      it.gsub!(/WIDGETNAME/, @widgetname)
      it.gsub!(/CLASSNAME/, @classname)
      # http://www.ruby-doc.org/core/classes/String.html#M001186
      # Stick a block on the end of these.
      it.gsub!(/WIDGETDESCRIPTION/, @widgetdescription)
      it.gsub!(/TITLE/, @default_title)
      it.gsub!(/DEMOTEXT/, @demotext)
      
      it.gsub!(/PLUGINURI/, @pluginuri)
      it.gsub!(/PLUGINDESCRIPTION/, @plugindescription)
      it.gsub!(/PLUGINAUTHOR/, @pluginauthor)
      it.gsub!(/AUTHORURI/, @pluginauthoruri)
      it.gsub!(/DISPLAYCODE/, @plugindisplay)
      
      # Add a gsub and block for text input fields, etc.
      puts it
    end
=begin
    f.pos = 0                     # back to start
    f.print lines                 # write out modified lines
    f.truncate(f.pos)             # truncate to new length
=end
  end
  
end

