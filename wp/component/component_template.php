<?php
/**
 * @description
 * @link
 * @return String containing div component which may
 * be echoed by the invoking code.
 */
function NAMESPACE_get_COMPONENTNAME() {
  
  $content = <<<EOC
<div id="COMPONENTNAME-id" class="COMPONENTNAME-class">
  <hr />
  COMPONENTNAME content goes here.
</div>
EOC;
  return $content;
}

function NAMESPACE_echo_COMPONENTNAME() {
  echo NAMESPACE_get_COMPONENTNAME();
}
add_filter('COMPONENTNAME-component', 'NAMESPACE_echo_COMPONENTNAME');

?>