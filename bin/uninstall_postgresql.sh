#!/bin/bash

echo "Stopping PostgreSQL services via Homebrew..."
brew services stop postgresql@14 || echo "postgresql@14 not running"
brew services stop postgresql@16 || echo "postgresql@16 not running"
brew services stop postgresql@15 || echo "postgresql@15 not running"
brew services stop postgresql@17 || echo "postgresql@17 not running"

echo "Unlinking PostgreSQL..."
brew unlink postgresql@14 || echo "postgresql@14 not linked"
brew unlink postgresql@16 || echo "postgresql@16 not linked"
brew unlink postgresql@15 || echo "postgresql@15 not linked"
brew unlink postgresql@17 || echo "postgresql@17 not linked"

echo "Uninstalling PostgreSQL via Homebrew..."
brew uninstall --force postgresql postgresql@14 postgresql@15 postgresql@16 postgresql@17 || echo "No PostgreSQL versions installed"

echo "Uninstalling PostgreSQL extensions (like pgvector)..."
brew uninstall --force pgvector || echo "pgvector not installed"
sudo rm -rf /opt/homebrew/share/postgresql@*/extension/*

echo "Removing leftover PostgreSQL directories and configuration files..."
sudo rm -rf /opt/homebrew/var/postgresql*
sudo rm -rf /opt/homebrew/etc/postgresql*
sudo rm -rf /opt/homebrew/lib/postgresql*
sudo rm -rf /opt/homebrew/include/postgresql*
sudo rm -rf /opt/homebrew/share/postgresql*
sudo rm -rf /opt/homebrew/opt/postgresql*
sudo rm -rf /opt/homebrew/Cellar/postgresql*
sudo rm -rf /opt/homebrew/var/homebrew/locks/postgresql*
sudo rm -rf /opt/homebrew/var/homebrew/locks/pgvector*


# This script run to completion removes all traces
# of postrges, which could break a lot of other tools.
# Proceed with care from here.
exit

sudo rm -rf ~/Library/Application\ Support/Postgres

echo "Clearing Homebrew cache and logs..."
rm -rf ~/Library/Caches/Homebrew/postgresql*
rm -rf ~/Library/Logs/Homebrew/postgresql*
rm -rf ~/Library/Logs/Homebrew/pgvector*

echo "Removing PostgreSQL users and groups (if they exist)..."
sudo dscl . -delete /Users/postgres 2>/dev/null || echo "No postgres user found"
sudo dscl . -delete /Groups/postgres 2>/dev/null || echo "No postgres group found"

echo "Removing PostgreSQL binaries..."
sudo rm -f /usr/local/bin/psql /usr/local/bin/pg_ctl /usr/local/bin/initdb
sudo rm -f /opt/homebrew/bin/psql /opt/homebrew/bin/pg_ctl /opt/homebrew/bin/initdb
sudo rm -f /usr/bin/psql /usr/bin/pg_ctl /usr/bin/initdb

echo "Removing old PostgreSQL data from user directories..."
rm -rf ~/.psql_history
rm -rf ~/.pgpass
rm -rf ~/.postgresql

echo "Ensuring no PostgreSQL processes are running..."
pgrep postgres && pkill -9 postgres || echo "No PostgreSQL processes running."

echo "Final cleanup: searching for any remaining PostgreSQL files..."
sudo find / -name "postgres*" -exec rm -rf {} \; 2>/dev/null

echo "PostgreSQL uninstallation complete. System is clean."


echo "Stopping PostgreSQL services via Homebrew..."
brew services stop postgresql@14 || echo "postgresql@14 not running"
brew services stop postgresql@16 || echo "postgresql@16 not running"

echo "Unlinking PostgreSQL..."
brew unlink postgresql@14 || echo "postgresql@14 not linked"
brew unlink postgresql@16 || echo "postgresql@16 not linked"

echo "Uninstalling PostgreSQL via Homebrew..."
brew uninstall --force postgresql@14 || echo "postgresql@14 not installed"
brew uninstall --force postgresql@16 || echo "postgresql@16 not installed"
brew uninstall --force postgresql postgresql@15 postgresql@17 || echo "Other PostgreSQL versions not installed"

brew uninstall --force pgvector || echo "pgvector not installed"
sudo rm -rf /opt/homebrew/share/postgresql@*/extension/*


echo "Removing leftover directories and configuration files..."
sudo rm -rf /opt/homebrew/var/postgresql@14
sudo rm -rf /opt/homebrew/var/postgresql@16
sudo rm -rf /opt/homebrew/var/homebrew/locks/postgresql@14.formula.lock
sudo rm -rf /opt/homebrew/var/homebrew/locks/postgresql@16.formula.lock
sudo rm -rf /opt/homebrew/var/log/postgresql@16.log

echo "Clearing Homebrew cache and logs..."
rm -rf ~/Library/Caches/Homebrew/postgresql@14*
rm -rf ~/Library/Caches/Homebrew/postgresql@16*
rm -rf ~/Library/Logs/Homebrew/postgresql@14
rm -rf ~/Library/Logs/Homebrew/postgresql@16

sudo rm -rf /opt/homebrew/var/homebrew/locks/postgresql*
sudo rm -rf /opt/homebrew/var/homebrew/locks/pgvector*

sudo dscl . -delete /Users/postgres 2>/dev/null || echo "No postgres user found"
sudo dscl . -delete /Groups/postgres 2>/dev/null || echo "No postgres group found"

sudo rm -f /usr/local/bin/psql
sudo rm -f /opt/homebrew/bin/psql
sudo rm -f /usr/bin/psql

sudo rm -f /usr/local/bin/pg_ctl
sudo rm -f /usr/local/bin/initdb
sudo rm -f /opt/homebrew/bin/pg_ctl
sudo rm -f /opt/homebrew/bin/initdb

pgrep postgres && pkill -9 postgres || echo "No PostgreSQL processes running."


echo "Verifying no PostgreSQL binaries are in PATH..."
if which psql > /dev/null 2>&1; then
  echo "Removing PostgreSQL binaries..."
  sudo rm -f $(which psql)
else
  echo "No PostgreSQL binaries found in PATH."
fi

# echo "Final cleanup: searching for any remaining PostgreSQL files..."
# sudo find / -name "postgres*" -exec rm -rf {} \; 2>/dev/null

echo "PostgreSQL uninstallation complete. System is clean."

