!#/bin/bash

task add due:today priority:H +aptible "Login to dashlane"
task add due:today priority:H +aptible "Login to apitble cli"
task add due:today priority:H +aptible "Login to aptible console"
task add due:today priority:H +aptible "Start datagrip to find correct pg port"
task add due:today priority:H +aptible "Tunnel: aptible db:tunnel healthie-staging-14 --port=65338 # port from datagrip"
task add due:today priority:H +aptible "Console: aptible ssh --app healthie2.0-staging"
