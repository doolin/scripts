!#/bin/bash

task add due:today priority:C +hiring "Evaluate $1 Ruby prime number"
task add due:today priority:C +hiring "Evaluate $1 class design"
task add due:today priority:C +hiring "Evaluate $1 schema"
task add due:today priority:C +hiring "Evaluate $1 short answer"
