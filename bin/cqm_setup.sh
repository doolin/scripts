!#/bin/bash

task add due:today priority:C +onc +cqm "As Admin, log in to CQM Solution: https://healthiedev.cqmsolution.com/Account/login.aspx"
task add due:today priority:C +onc +cqm "As Clinician, log in to CQM Solution: https://healthiedev.cqmsolution.com/Account/login.aspx"
task add due:today priority:C +onc +cqm "Log in to Drummond test site: https://cypress702.dg-testlab.com/vendors"
task add due:today priority:C +onc +cqm "Open criteria_list.html in a browser window"

# task add due:today priority:C +onc +cqm "Log in to Dosespot staging: https://stage-pss.dosespot.com/Admin/"
