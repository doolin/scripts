!#/bin/bash

task add due:today priority:H +erx "Start localhost servers"
task add due:today priority:H +erx "Login to localhost"
task add due:today priority:H +erx "Login to staging6"
task add due:today priority:H +erx "Log in to NCPDP https://tools.ncpdp.org/erx/#/cb"
task add due:today priority:H +erx "Log in to Dosespot customer support: https://dosespot.force.com/Customers/s/case/"
task add due:today priority:H +erx "Log in to Dosespot staging: https://stage-pss.dosespot.com/Admin/"
