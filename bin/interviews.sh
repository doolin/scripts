
if [ $# -eq 0 ]
  then
    echo "Please specify a candidate by name"
    exit
fi

task add due:today +hiring "Interview prep $1"
task add due:today +hiring "Interview $1"
task add due:today +hiring "Interview writeup $1"
