#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "Please specify a CQM measure"
    exit
fi

task add due:today priority:C +onc "Find $1 section in proctor sheet"
task add due:today priority:C +onc "Create a Shortcut ticket for $1 with measure title"
task add due:today priority:C +onc "Add proctor sheet description for measure on Shortcut ticket for $1"
task add due:today priority:C +onc "Add link with page number for measure on Shortcut ticket for $1"
task add due:today priority:C +onc "Add Acceptance Criteria on Shortcut ticket for $1"
task add due:today priority:C +onc "Add Tech Details for on Shortcut ticket $1"